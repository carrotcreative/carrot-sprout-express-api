path = require('path')
S = require('underscore.string')

module.exports = (utils, name) ->

  ###*
  * Read the model stub.
  ###

  utils.src.read(path.join('stubs', 'model.coffee'))
    .then (src) ->

      ###*
      * Write the new model.
      ###

      tgt = path.join('lib', 'models', "#{S.slugify(name)}.coffee")
      utils.target.write(tgt, src, name: name)

    .then ->

      ###*
      * Read the CRUD routes stub
      ###

      utils.src.read(path.join('stubs', 'crud.coffee'))

    .then (src) ->

      ###*
      * Write the new CRUD routes.
      ###

      tgt = path.join('lib', 'routes', "#{S.slugify(name)}.coffee")
      utils.target.write(tgt, src, name: name)
