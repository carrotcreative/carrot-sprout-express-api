path = require 'path'
S = require 'underscore.string'

module.exports = (utils, name) ->

  ###*
  * Read the routes stub.
  ###

  utils.src.read(path.join('stubs', 'routes.coffee'))
    .then (src) ->

      ###*
      * Write the new routes file.
      ###

      tgt = path.join('lib', 'routes', "#{S.slugify(name)}.coffee")
      utils.target.write(tgt, src, name: name)
