# sprout-express-api
a [sprout](https://github.com/carrot/sprout) (>= 0.4.0) template for an API built with express

### Installation
- `npm install sprout-cli -g`
- `sprout add api https://github.com/carrot/sprout-express-api.git`
- `sprout new api myexpressapi`

### What does this template provide you with?
- [x] an easy to follow app structure
- [x] built with [coffeescript](http://coffeescript.org/)
- [x] control for multiple environments [dev, staging, production]
- [x] all logic is [promise](https://promisesaplus.com/) based
- [x] object modeling with [mongoose](http://mongoosejs.com/)
- [x] cross-origin support  
- [x] easy local development via [nodemon](http://nodemon.io/)
- [x] easy deployment to [heroku](http://heroku.com/)
- [x] model generators run on init
- [x] controller generators run on init
- [ ] a unit test harness powered by [chai](http://chaijs.com/) and [supertest](https://github.com/visionmedia/supertest)

### Options
- **name** (name of template)
- **models** (a list of your models)
