ModelsGenerator = require './generators/model'
RoutesGenerator = require './generators/routes'
path            = require 'path'
slugify         = require 'underscore.string/slugify'
Promise         = require 'bluebird'

exports.configure = [
  {
    name: 'name'
    message: 'What is the name of your project?'
  },
  {
    name: 'models'
    message: 'List your models (separate with spaces)'
  },
  {
    name: 'routes'
    message: 'List your routes files (separate with spaces)'
  }
]

exports.beforeRender = (utils, config) ->
  config.name = slugify(config.name)
  config.models = config.models.split(/\s+/)
  config.routes = config.routes.split(/\s+/)

exports.after = (utils, config) ->
  tgt = slugify(config.name)
  utils.target.rename(path.join('bin', 'start'), path.join('bin', tgt))
  Promise.map(config.models, (model) -> ModelsGenerator(utils, model))
    .then(-> Promise.map(config.routes, (routes) -> RoutesGenerator(utils, routes)))
