body_parser = require 'body-parser'
morgan      = require 'morgan'

module.exports = (app) ->

  app.use(body_parser.json())
  app.use(body_parser.urlencoded(extended: true))
  app.use(morgan('dev'))
  app.use (response, request, next) ->
    response.header('Access-Control-Allow-Origin', '*')
    next()
