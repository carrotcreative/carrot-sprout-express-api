config = require('./config').mongo
mongoose = require('mongoose')

connection = mongoose.createConnection(config.url)
connection.on 'error', (error) -> console.log(error)

module.exports = connection
