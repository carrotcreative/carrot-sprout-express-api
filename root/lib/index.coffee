express = require('express')

module.exports = (port, callback) ->

  app = express()
  
  require('./app')(app)
  require('./routes')(app)

  app.listen port, ->
    callback(null, port) if 'function' is typeof callback
