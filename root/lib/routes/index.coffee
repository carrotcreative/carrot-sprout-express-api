routes    = require('indx')(__dirname)
classify  = require('underscore.string/classify')

module.exports = (app) ->
  exports[classify(name)] = route(app) for name, route of routes
