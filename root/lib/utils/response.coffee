exports.onCompletion = (response) ->
  (error, result) ->
    if error then response.status(500).json(error) else response.json(result)
