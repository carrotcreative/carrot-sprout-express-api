config = require('./../config')
_ = require('lodash')

conf = config.base or {}
env = process.env.NODE_ENV or 'development'

config = config or {}
conf = _.merge(conf, config[env]) if config[env]

conf.env = env

module.exports = conf
