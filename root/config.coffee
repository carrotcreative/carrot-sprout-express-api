module.exports =
  base:
    mongo:
      url: 'mongodb://localhost/<%= name %>'
  production:
    mongo:
      url: process.env.MONGOSOUP_URL
