mongo = require('./../mongo')
mongoose = require('mongoose')

<%= S.camelize(name) %> = new mongoose.Schema({

  ###*
  *
  * Add Mongoose schema here.  For example:
  *
  * name:
  *  type: String,
  *  required: 'Name is required!'
  *
  * Mongoose schema docs: http://mongoosejs.com/docs/guide.html
  *
  ###

})

module.exports = mongo.model('<%= S.camelize(name) %>', <%= S.camelize(name) %>)
