Models = require('./../models')
utils  = require('./../utils')

module.exports = (app) ->

  app.get '/<%= S.slugify(name) %>', (request, response) ->
    Models.<%= S.classify(name) %>
      .find()
      .exec(utils.response.onCompletion(response))

  app.post '/<%= S.slugify(name) %>', (request, response) ->
    Models.<%= S.classify(name) %>
      .create(request.params.body, utils.response.onCompletion(response))

  app.get '/<%= S.slugify(name) %>/:id', (request, response) ->
    Models.<%= S.classify(name) %>
      .findOne(_id: request.params.id)
      .exec(utils.response.onCompletion(response))

  app.put '/<%= S.slugify(name) %>/:id', (request, response) ->
    Models.<%= S.classify(name) %>
      .update({_id: request.params.id}, request.params.body, utils.response.onCompletion(response))

  app.delete '/<%= S.slugify(name) %>/:id', (request, response) ->
    Models.<%= S.classify(name) %>
      .remove({_id: request.params.id}, utils.response.onCompletion(response))
